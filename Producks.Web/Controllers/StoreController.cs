﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Producks.Data;
using Producks.Web.Models;

/*
- Add a new controller called StoreController [DONE]
- scaffold it as an Empty MVC Controller. [DONE]
- Add an Index method to the controller [DONE]
- (and an associated View to the Project) to display a list of categories [DONE]
- Add the capability to drill-down from a category into the associated products – this will require a link in the Index view to a new Action that displays
  products-by-category, which itself will require another View adding to the project –this time you can use the existing ProductsController for inspiration.
  Remember to tweak the views to take into account that all products have the same category. [DONE]
  TODO: Might have to make a new DTO for DrillDown
  TODO: Ask James/Tyrone if seperate DTOs for admin/clients are acceptable
*/

namespace Producks.Web.Controllers
{
    public class UcBrandDto : BrandDto
    {
        public int AvailabeProductCount { get; set; }
    }

    public class UcProductDto : ProductDto
    {
        //public int AvailabeProductCount { get; set; }
        //public int Id { get; set; }
        //public string Name { get; set; }
        //public int BrandId { get; set; }
        //public string BrandName { get; set; }
        //public int CategoryId { get; set; }
        public string CategoryName { get; set; }
        //public string Description { get; set; }
        //public string Ean { get; set; }
        //public bool ExpectedRestock { get; set; }
        //public bool InStock { get; set; }
        //public double Price { get; set; }
    }

    public class StoreDto
    {
        public List<Category> categories { get; set; }
        public List<Brand> brands { get; set; }
    }

    public class StoreController : Controller
    {
        private readonly Data.StoreDb _context;

        private HttpClient client = new HttpClient();

        public StoreController(Data.StoreDb context)
        {
            _context = context;
            client.BaseAddress = new Uri("http://undercutters.azurewebsites.net"); // http://undercutters.azurewebsites.net/api/Category
            client.DefaultRequestHeaders.Accept.Clear(); //TODO: See if this has any effect, if none remove it
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
        }

        //TODO: return correct type 
        public async Task<IActionResult> Index()
        {
            //Get Categories
            List<Category> categories = new List<Category>();
            categories = await _context.Categories
                .Where(c => c.Active)
                .Select(c => new Category
                {
                    Id = c.Id,
                    Name = c.Name,
                    Description = c.Description
                })
                .ToListAsync();

            HttpResponseMessage res = await client.GetAsync("api/Category");
            var cat = await res.Content.ReadAsAsync<List<UcCategoryDto>>();

            categories.AddRange(cat.Select(c => new Category {
                Id = c.Id,
                Name = c.Name,
                Description = c.Description
            }));

            //Get Brands
            List<Brand> brands = new List<Brand>();
            brands = await _context.Brands
                .Where(c => c.Active)
                .Select(c => new Brand
                {
                    Id = c.Id,
                    Name = c.Name
                })
                .ToListAsync();

            res = await client.GetAsync("api/Brand");
            var brand = await res.Content.ReadAsAsync<List<UcBrandDto>>();

            brands.AddRange(brand.Select(c => new Brand
            {
                Id = c.Id,
                Name = c.Name
            }));

            //Return StoreDto
            StoreDto sd = new StoreDto();
            sd.categories = categories;
            sd.brands = brands;

            return View(sd);
        }

        public async Task<IActionResult> CatDrillDown(int id, string Name)
        {
            List<Product> products = await _context.Products
                .Where(p => p.Active)
                .Where(p => p.CategoryId == id)
                .Select(p => new Product {
                    Id = p.Id,
                    Name = p.Name,
                    Description = p.Description,
                    Price = p.Price
                })
                .ToListAsync();

            HttpResponseMessage res = await client.GetAsync("api/Product");
            var UcProducts = await res.Content.ReadAsAsync<List<UcProductDto>>();

            products.AddRange(UcProducts
                .Where(p => Name == p.CategoryName)
                .Select(p => new Product
                {
                    Id = p.Id,
                    Name = p.Name,
                    Description = p.Description,
                    Price = p.Price
                }));

            return View("DrillDown", products);
        }

        public async Task<IActionResult> BrandDrillDown(int id)
        {
            List<Product> products = await _context.Products
                .Where(p => p.Active)
                .Where(p => p.BrandId == id)
                .Select(p => new Product
                {
                    Id = p.Id,
                    Name = p.Name,
                    Description = p.Description,
                    Price = p.Price
                })
                .ToListAsync();

            HttpResponseMessage res = await client.GetAsync("api/Product");
            var UcProducts = await res.Content.ReadAsAsync<List<UcProductDto>>();

            products.AddRange(UcProducts
                .Select(p => new Product
                {
                    Id = p.Id,
                    Name = p.Name,
                    Description = p.Description,
                    Price = p.Price
                }));

            return View("DrillDown", products);
        }

        //public async Task<List<UcCategoryDto>> GetUcCategories()
        //{
        //    List<UcCategoryDto> cat = null;

        //    HttpResponseMessage res = await client.GetAsync("api/Category");
        //    if(res.IsSuccessStatusCode)
        //    {
        //        cat = await res.Content.ReadAsAsync<List<UcCategoryDto>>();
        //    }

        //    return cat;
        //}

        public async Task<List<UcBrandDto>> GetUcBrands()
        {
            List<UcBrandDto> brand = null;

            HttpResponseMessage res = await client.GetAsync("api/Brand");
            if (res.IsSuccessStatusCode)
            {
                brand = await res.Content.ReadAsAsync<List<UcBrandDto>>();
            }

            return brand;
        }

        public async Task<List<UcProductDto>> GetUcProducts()
        {
            List<UcProductDto> prod = null;

            HttpResponseMessage res = await client.GetAsync("api/Product");
            if (res.IsSuccessStatusCode)
            {
                prod = await res.Content.ReadAsAsync<List<UcProductDto>>();
            }

            return prod;
        }
    }
}