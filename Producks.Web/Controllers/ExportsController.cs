﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Producks.Data;
using Producks.Web.Models;

namespace Producks.Web.Controllers
{
    [ApiController]
    public class ExportsController : ControllerBase
    {
        private readonly StoreDb _context;

        public ExportsController(StoreDb context)
        {
            _context = context;
        }

        // GET: api/Brands
        [HttpGet("api/Brands")]
        public async Task<IActionResult> GetBrands()
        {
            var brands = await _context.Brands
                                       .Where(b => b.Active)
                                       .Select(b => new BrandDto
                                       {
                                           Id = b.Id,
                                           Name = b.Name
                                       })
                                       .ToListAsync();
            return Ok(brands);
        }

        // GET: api/categories
        [HttpGet("api/Categories")]
        public async Task<IActionResult> GetCategories()
        {
            var categories = await _context.Categories
                                       .Where(b => b.Active)
                                       .Select(b => new CategoryDto
                                       {
                                           Id = b.Id,
                                           Name = b.Name,
                                           Description = b.Description
                                       })
                                       .ToListAsync();
            return Ok(categories);
        }

        // GET: api/products
        // /api/products?priceFrom=1&priceTo=3
        [HttpGet("api/Products")]
        public async Task<IActionResult> GetProducts(double? priceFrom, double? priceTo)
        {
            var products = await _context.Products
                .Where(p => p.Active)
                .Where(p => !priceFrom.HasValue || (priceFrom.HasValue && p.Price >= priceFrom))
                .Where(p => !priceTo.HasValue || (priceTo.HasValue && p.Price <= priceTo))
                .Select(b => new ProductDto
                {
                    Id = b.Id,
                    Name = b.Name,
                    Description = b.Description
                })
                .ToListAsync();
            return Ok(products);
        }

        [HttpGet("api/ProductsByBrand")]
        public async Task<IActionResult> GetProducts(int brandId)
        {
            var products = await _context.Products
                .Where(p => p.Active)
                .Where(p => p.BrandId == brandId)
                .Select(b => new ProductDto
                {
                    Id = b.Id,
                    Name = b.Name,
                    Description = b.Description
                })
                .ToListAsync();
            return Ok(products);
        }

        [HttpGet("api/ProductsByCategory")]
        public async Task<IActionResult> GetProductsByCat(int catId)
        {
            var products = await _context.Products
                .Where(p => p.Active)
                .Where(p => p.CategoryId == catId)
                .Select(b => new ProductDto
                {
                    Id = b.Id,
                    Name = b.Name,
                    Description = b.Description
                })
                .ToListAsync();
            return Ok(products);
        }
    }
}
