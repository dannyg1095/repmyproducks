﻿using Producks.Web.Controllers;
using System;
using System.Runtime.Serialization;

namespace Producks.Web.Models
{
    public class UcCategoryDto
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public int AvailableProductCount { get; set; }
    }
}
